#include<stdio.h>
#include<wait.h>
#include<signal.h>
#include<sys/types.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>
#include<fcntl.h>
void usage(const char *arg){
	printf("%s <host> <puerto>\n",arg);
	exit(1);
}
void error(int codigo, const char *err){
	switch(codigo){
		case 0: printf("La direcci�n del Host no es v�lida\n"); break;
		case 1: printf("Error conectando con el host %s\n",err); break;
		case 2: printf("Error enviando los datos a %s\n",err); break;
	}
exit(1);
}
char *cmp(char *palabra, char *nombre){
        char *mem=(char *)calloc(220,sizeof(char));
        strcat(mem,nombre);
        strcat(mem,": ");
        strcat(mem,palabra);
return mem;
}
void leer(int);
int escribir(int,struct sockaddr_in);
int conectar(int, struct sockaddr_in);
int main(int argc, char **argv){
	if(argc<3)
		usage(argv[0]);
	int conexion, puerto;
	struct sockaddr_in cliente;
	struct hostent *servidor;
	servidor=gethostbyname(argv[1]);
	if(servidor==NULL)
		error(0," ");
	conexion=socket(AF_INET,SOCK_STREAM,0);
	puerto=atoi(argv[2]);
	cliente.sin_family=AF_INET;
	cliente.sin_port=htons(puerto);
	bcopy((char *)servidor->h_addr,(char *)&cliente.sin_addr,sizeof(servidor->h_length));
	conectar(conexion, cliente);
	send(conexion,"78421687541295",15,0);
	close(conexion);
return 0;
}
int conectar(int sock, struct sockaddr_in client){
	int c;
	int id;
	int estado;
	if(connect(sock,(struct sockaddr *)&client,sizeof(client))<0)
		error(1,inet_ntoa(client.sin_addr));
	else
		printf("Conectando con %s:%d\n\n",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
	printf("Escribe \"EOF\" para salir\n");
	printf("\t\t\t\t%s\n",inet_ntoa(client.sin_addr));
	fcntl(sock,F_SETFL,O_NONBLOCK);
	id=fork();
	if(id==0){
		while(1){
			leer(sock);
			sleep(1);
		}
	}
	while(1){
		c=escribir(sock,client);
		if(c==1)
			break;
	}
	kill(id,SIGKILL);
return 0;
}
void leer(int conexion){
	char *cadena;
	cadena=(char *)calloc(220,sizeof(char));
	if(recv(conexion,cadena,220,0)>0)
		printf("\n%s",cadena);
	else
		free(cadena);
}
int escribir(int conexion,struct sockaddr_in client){
	char *buffer=(char *)calloc(220,sizeof(char));
	char *cadena=(char *)calloc(200,sizeof(char));
	printf("%s: ",getlogin());
	fgets(cadena,200,stdin);
	buffer=cmp(cadena,getlogin());
	if(strcmp(cadena,"EOF\n")==0)
		return 1;
	if(send(conexion,buffer,220,0)<0)
		error(2,inet_ntoa(client.sin_addr));
	free(buffer);
	free(cadena);
return 0;
}
