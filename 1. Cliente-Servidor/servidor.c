#include<stdio.h>
#include<wait.h>
#include<sys/types.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include<fcntl.h>
void usage(const char *arg){
	printf("%s <puerto>\n",arg);
exit(1);
}
char *cmp(char *palabra, char *nombre){
        char *mem=(char *)calloc(220,sizeof(char));
        strcat(mem,nombre);
        strcat(mem,": ");
        strcat(mem,palabra);
return mem;
}
void escribir(int);
int recibir(int);
int escuchar(int, struct sockaddr_in);
int main(int argc, char **argv){
	if(argc<2)
		usage(argv[0]);
	int conexion_servidor, puerto;
	char *buffer;
	struct sockaddr_in servidor;
	puerto=atoi(argv[1]);
	conexion_servidor=socket(AF_INET,SOCK_STREAM,0);
	servidor.sin_family=AF_INET;
	servidor.sin_port=htons(puerto);
	servidor.sin_addr.s_addr=INADDR_ANY;
	escuchar(conexion_servidor,servidor);
	printf("Hasta luego\n");
	close(conexion_servidor);
return 0;
}
int escuchar(int conexion_servidor, struct sockaddr_in servidor){
	struct sockaddr_in cliente;
	int c;
	int id;
	int conexion_cliente;
	int clilong;
	if(bind(conexion_servidor,(struct sockaddr *)&servidor,sizeof(servidor))<0){
                printf("Error. El puerto est� en uso\n");
                close(conexion_servidor);
                return 1;
        }
        listen(conexion_servidor,5);
        clilong=sizeof(cliente);
        printf("Escuchando en el puerto %d\n",htons(servidor.sin_port));
        conexion_cliente=accept(conexion_servidor,(struct sockaddr *)&cliente,&clilong);
        if(conexion_cliente<0){
                printf("Error en la asociacion con el host %s\n",inet_ntoa(cliente.sin_addr));
                close(conexion_servidor);
                return 1;
        }
        else
                printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),ntohs(servidor.sin_port));
	fcntl(conexion_cliente,F_SETFL,O_NONBLOCK);
	id=fork();
	if(id==0){
		while(1){
			sleep(1);
			c=recibir(conexion_cliente);
			if(c==1)
				break;
		}
	}
	while(c!=1)
		escribir(conexion_cliente);
	kill(id,SIGKILL);
return 0;
}
int recibir(int conexion){
	int c=0;
	char *buffer=(char *)calloc(220,sizeof(char));
	if(recv(conexion,buffer,220,0)>0){
		if(strcmp(buffer,"78421687541295")!=0)
			printf("\n%s",buffer);
		else
			c=1;
	}
	free(buffer);
return c;
}
void escribir(int conexion){
	char *buffer=(char *)calloc(220,sizeof(char));
	char *cadena=(char *)calloc(200,sizeof(char));
	printf("SERVIDOR: ");
	fgets(cadena,200,stdin);
	buffer=cmp(cadena,"SERVIDOR");
	if(send(conexion,buffer,220,0)<0)
		printf("Error enviando la informaci�n\n");
}
