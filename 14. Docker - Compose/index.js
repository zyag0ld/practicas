const mongoose = require("./mongo-init");
var express = require("express");
var app = express();

const Schema = mongoose.Schema;
const ANY = new Schema(
  {
    texto: { type: String },
    longitud:{type: String},
    Nc:{type: String},
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  },
  { strict: false, versionKey: false }
);

const saludoModel = mongoose.model("saludos", ANY);
app.get("/", function (req, res) {
  res.send("Estás en el root");
});
app.get("/saludo", async function (req, res) {
  let r = await saludoModel
    .find({})
    .limit(1)
    .sort({ createdAt: "desc" })
    .exec();
  res.json(r);
});
app.get("/saludos", async function (req, res) {
  let r = await saludoModel.find({}).sort({ createdAt: "desc" }).exec();
  res.json(r);
});

app.get("/saludos/eliminar/todos", async function (req, res) {
  saludoModel.remove({}, function (err) {
    res.send("Hecho");
  });
});
app.get("/saludo/crear", async function (req, res) {
  let r = await saludoModel.create({ texto: req.query.texto });
  res.json(r.toObject());
});

//Stuker
app.get("/saludo/primeros", async function (req, res) {
  let r = await saludoModel
	.find({})
	.limit(3)
	.sort({ createdAt: "asc" })
	.exec();
  res.json(r);
});
app.get("/saludo/ultimos", async function (req, res) {
  let r = await saludoModel
	.find({})
	.limit(3)
	.sort({ createdAt: "desc" })
	.exec();
  res.json(r);
});

//Zya Gold
app.get("/saludo/primeros/tres", async function (req, res) {
  let r = await saludoModel
	.find({})
	.limit(3)
	.sort({ createdAt: "asc" })
	.exec();
  res.json(r);
});
app.get("/saludo/ultimos/tres", async function (req, res) {
  let r = await saludoModel
	.find({})
	.limit(3)
	.sort({ createdAt: "desc" })
	.exec();
  res.json(r);
});

//SERVICIO
app.get("/saludo/crear_c", async function (req, res) {
  let r = await saludoModel.create({ texto: req.query.texto, longitud: req.query.texto.length });
  res.json(r.toObject());
});

app.listen(3000, function () {
  console.log("Servidor listo en el puerto 3000");
});