<?php
/*
 * Biblioteca de operaciones CRUD
*/

function get() {
    if($met == 'GET') {
        http_response_code(203);
    
        $servername = "mysql1004.mochahost.com";
        $username = "nsierrar_sd2020";
        $password = "n513rr4r5d";
        $dbname = "nsierrar_sd2020";
    
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
    
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
    
        $sql = "SELECT * FROM books";
        $result = mysqli_query($conn, $sql);
    
        $res=array();
    
        if (mysqli_num_rows($result) > 0) {
          while($row = mysqli_fetch_assoc($result)) {
              $res[]=array(
                      "id" => $data->id,
                      "name" => $data->name,
                      "description" => $data->description,
                      "price" => $data->price
                  );
          }
          $message="Data shown successfully";
        } else {
            //echo "0 results";
        }
            
        $conn->close();        
    
        echo json_encode(array(
            "method:" => $met,
            "message" => $message,
            "result" => $res,
            )
        );
        }
    
    else{
        http_response_code(503);
        echo json_encode(array("message" => "No data was received"));
    }
}

function receive() {
    if(
        !empty($data->id) &&
        !empty($data->name) &&
        !empty($data->description) &&
        !empty($data->price)
    ){
        http_response_code(201);
    
        echo json_encode(array(
            "method:" => $met,
            "message" => "Product was created.",
            "data-receive" => array(
              "id" => $data->id,
              "name" => $data->name,
              "description" => $data->description,
              "price" => $data->price
            )
          )
        );
    }else{
        http_response_code(503);
        echo json_encode(array("message" => "No data was received"));
    }
}

function transact() {
    if(
        !empty($data->id) &&
        !empty($data->name) &&
        !empty($data->description) &&
        !empty($data->price)
    ){
    
      if($met == 'POST'){
    
        http_response_code(201);
    
        $servername = "mysql1004.mochahost.com";
        $username = "nsierrar_sd2020";
        $password = "n513rr4r5d";
        $dbname = "nsierrar_sd2020";
    
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
    
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
    
        $sql = "INSERT INTO books (id, name, description, price, eliminado)
        VALUES ('".$data->id."','".$data->name."','".$data->description."','".$data->price."',0".")";
    
        if ($conn->query($sql) === TRUE) {
            $message="New record created successfully";
        } else {
            $message="Error: " . $sql . ", " . $conn->error;
        }
    
        $conn->close();
    
    
        echo json_encode(array(
            "method:" => $met,
            "message" => $message,
            "data-receive" => array(
              "id" => $data->id,
              "name" => $data->name,
              "description" => $data->description,
              "price" => $data->price
            )
          )
        );
      }
    }
    
    else{
        http_response_code(503);
        echo json_encode(array("message" => "No data was received"));
    }
}

function update() {
    if(
        !empty($data->id) &&
        !empty($data->name) &&
        !empty($data->description) &&
        !empty($data->price)
    ){
    if($met == 'PUT') {
        http_response_code(202);
        
        $servername = "mysql1004.mochahost.com";
        $username = "nsierrar_sd2020";
        $password = "n513rr4r5d";
        $dbname = "nsierrar_sd2020";
        
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        $sql = "UPDATE books SET name = '".$data->name."', description = '".$data->description."',price = '".$data->price."'
        WHERE id = '".$data->id."' ";
        
        if ($conn->query($sql) === TRUE) {
            $message="Record updated successfully";
        } else {
            $message="Error: " . $sql . ", " . $conn->error;
        }
        
        $conn->close();
        
        
        echo json_encode(array(
            "method:" => $met,
            "message" => $message,
            "data-receive" => array(
                "id" => $data->id,
                "name" => $data->name,
                "description" => $data->description,
                "price" => $data->price        
            )
            )
        );
        }
        }
    
    else{
        http_response_code(503);
        echo json_encode(array("message" => "No data was received"));
    }
}

function selectData(){
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    echo "Connected successfully<br>";
    
    echo "Books' Records<br>";
    
    $sql = "SELECT id, name, description, price, eliminado FROM books";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            echo $row["id"]." - ".$row["name"]." - ".$row["description"]." - ".$row["price"]." - ".$row["eliminado"]."<br>";
        }
    } else {
        echo "0 results";
    }
    $conn->close();
}
?>